package models

import play.api.Logger
import java.util.UUID
import play.modules.reactivemongo.json.collection.JSONCollection
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.api.Play.current
import play.api.libs.json._
import utils.Formatters._
import dto.requests._
import scala.concurrent.Future
import com.github.nscala_time.time.Imports._
import utils.Constants._
import reactivemongo.core.commands.LastError

/**
 * Data Access Object for Truck resource
 */
object TruckDAO {
  
  /** The MongoDB collection for storing the Truck resource */
  private def trucksCollection: JSONCollection = ReactiveMongoPlugin.db.collection[JSONCollection](TRUCKS_COLLECTION)
  
  /**
   * CREATE: Inserts the truck into the given MongoDB collection
   */
  def save(truck: Truck): Future[Truck] = {
    trucksCollection.save(truck).map {
      case ok if ok.ok => truck
      case error => throw new RuntimeException(error.message)
    }
  }
  
  /**
   * READ: Returns the truck with the given id, if any. 
   */
  def findById(id: UUID): Future[Option[Truck]] = {
    trucksCollection.find(Json.obj("id" -> id)).one[Truck]
  }
  
  /**
   * DELETE: Deletes the truck with the given id from the trucksCollection, if present.
   */
  def delete(id: UUID): Future[reactivemongo.core.commands.LastError] = {
    trucksCollection.remove(Json.obj("id" -> id))
  }
  
  /**
   * SEARCH: Searches for all trucks around the given userLocation within the given radius, 
   * filtering them with the other search criteria.
   * 
   * @return Returns a list of trucks that match the search criteria
   */
  def search(req: TruckSearchRequest): Future[List[Truck]] = {
    
    /* Construct the search query */
    val dbQuery = buildSearchQuery(req.userGeocode, req.query, req.facilityType, req.period)
    
    // Return the trucks fetched from the DB after executing the search
    trucksCollection.find(dbQuery)
    .cursor[Truck]
    .collect[List]()
  }
  
  
  private def buildSearchQuery(userGeocode: UserGeocode,
                               textQuery: Option[String], 
                               facilityType: Option[String],
                               searchPeriod: Option[SearchPeriod]): JsObject = {
    
    /* 
     * 1. First search for all trucks within given radius around the user's location 
     */
    val searchRadiusInRadians = userGeocode.radiusInMiles/EARTH_RADIUS_IN_MILES
    
    var dbQuery: JsObject = Json.obj(
        "location" -> Json.obj(
            "$geoWithin" -> Json.obj(
                "$center" -> Json.arr(Json.arr(userGeocode.long, userGeocode.lat), searchRadiusInRadians)
            )
        )
    )
    
    /* 
     * 2. Now filter those results based on the user's text search query, if provided.
     */
    var jsonTransformer: Reads[JsObject] = null
    
    if(textQuery.isDefined) {
      //get the query string
      val q: String = utils.normalizedString(textQuery.get)
      
      if(!q.isEmpty()) {
        val textSearchFilter = Json.obj("$text" -> Json.obj("$search" -> q))
        
        val jsonTransformer = (__).json.update(
          __.read[JsObject].map { o => o ++ textSearchFilter }    
        )
        dbQuery = dbQuery.transform(jsonTransformer).getOrElse(dbQuery)
      }
    }
    
    /* 
     * 3. Filter the results based on the facilityType, if provided.
     */
    if(facilityType.isDefined) {
      val faclType: String = utils.normalizedString(facilityType.get)
      
      if(! faclType.isEmpty()) {
        val truckTypeFilter = Json.obj("facilityType" -> faclType)
      
        val jsonTransformer = (__).json.update(
          __.read[JsObject].map { o => o ++ truckTypeFilter }    
        )
        dbQuery = dbQuery.transform(jsonTransformer).getOrElse(dbQuery)
      }
    }
    
    /* 
     * 4. Filter the results based on the search time range provided by the user.
     *    i.e. Provide only the trucks which are open during the given time range.
     */
    
    if(searchPeriod.isDefined) {
      val searchByAvailabilityFilter = getSearchByAvailabilityFilter(searchPeriod.get.startTime, 
                                                                     searchPeriod.get.endTime)
      jsonTransformer = (__).json.update(
        __.read[JsObject].map { o => o ++ searchByAvailabilityFilter }    
      )
      dbQuery = dbQuery.transform(jsonTransformer).getOrElse(dbQuery)
    }
    
    
    Logger.info(s"Search query = ${dbQuery.toString()}")
    
    dbQuery
  }
  
  /*
   * Prepare and return a search filter to filter out the trucks 
   * that are not open at any point of time during the given search time range.
   */
  def getSearchByAvailabilityFilter(searchStartTime: Long, searchEndTime: Long): JsObject = {
    val searchStartHour = utils.getHourOfWeek(searchStartTime)
    val searchEndHour = utils.getHourOfWeek(searchEndTime)
    
    val searchByAvailabilityFilter = 
      if(searchStartHour <= searchEndHour) {
        /*
         * case 1: searchStartHourOfWeek <= searchEndHourOfWeek
         * Acceptable schedules can overlap anywhere in the range = [searchStartHour, searchEndHour] 
         */
        Json.obj(
            "schedules" -> Json.obj(
                "$elemMatch" -> Json.obj(
                    "endHourOfWeek" -> Json.obj("$gt" -> searchStartHour),
                    "startHourOfWeek" -> Json.obj("$lt" -> searchEndHour)
                )
            )
        )
      } else {
        /*
         * case 2: searchStartHourOfWeek > searchEndHourOfWeek
         * Acceptable schedules can overlap anywhere in the range = [searchStart, 168) union (0, searchEnd]
         * as a week has 168 hours
         */
        Json.obj(
            "schedules.endHourOfWeek" -> Json.obj("$gt" -> searchStartHour),
            "schedules.startHourOfWeek" -> Json.obj("$lt" -> searchEndHour)
        )
      }
    
    searchByAvailabilityFilter
  }
  
}
