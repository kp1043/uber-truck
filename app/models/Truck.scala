package models

import java.util.UUID
import play.api.libs.json._
import utils.Formatters._

/**
 * Defines a Truck
 */
case class Truck (
    id: UUID,
    locationId: String,
    name: String,
    facilityType: String,
    locationDescription: String,
    address: String,
    block: String,
    lot: String,
    permitId: String,
    permitStatus: PermitStatus.PermitStatus,
    foodItems: String,
    location: Point,
    schedules: List[Schedule]
) {
  
  def toJson: JsValue = Json.toJson(this)
}

/**
 * Represents a geographic point (longitude, latitude)
 */
case class Point(lon: Double, lat: Double)


/*
 * Truck availability schedules. Each truck can have a set of availability schedules for the week.
 * e.g. (126, 132) => Available from Friday: 6pm to 12noon
 */
case class Schedule (
    startHourOfWeek: Int,
    endHourOfWeek: Int
) {
  
  //Validations for the fields
  require(utils.isValidHourOfWeek(startHourOfWeek), "startHourOfWeek must be in [0,168]")
  require(utils.isValidHourOfWeek(endHourOfWeek), "endHourOfWeek must be in [0,168]")
  
  def toJson: JsValue = Json.toJson(this) 
}


/** The truck permit status */
object PermitStatus extends Enumeration {
  type PermitStatus = String
  val SUSPEND, APPROVED, REQUESTED, EXPIRED = Value
}
