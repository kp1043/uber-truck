import play.api.libs.json._
import utils.Formatters._
import com.github.nscala_time.time.Imports._
import play.api.Logger
import org.joda.time.DateTimeConstants.{ MILLIS_PER_HOUR, HOURS_PER_DAY, HOURS_PER_WEEK }

/**
 * Package object for utility methods
 */
package object utils {
  
  /**
   * The Error Response returned by API end-points
   */
  case class ErrorResponse(httpCode: Int, errMsg: String) {
    def toJson: JsValue = Json.toJson(this)
  }
  
  def normalizedString(str: String): String = {
    str.trim().toLowerCase()
  }
  
  /** Returns true if the given hour is in the range [0,168) */
  def isValidHourOfWeek(hour: Int): Boolean = {
    hour>=0 && hour<=HOURS_PER_WEEK
  }

  def getHourOfWeek(millis: Long) = {
    val dt = new DateTime(millis, DateTimeZone.UTC)
    val hourOfDay = dt.hourOfDay().get  //in range [0,23]
    val dayOfWeek = dt.dayOfWeek().get	//in range [1,7]
    
    val hourOfWeek = ((dayOfWeek - 1) * HOURS_PER_DAY) + hourOfDay  //in range [1,168)
    assert(isValidHourOfWeek(hourOfWeek), "Invalid hour of week")
    
    hourOfDay
  }

}