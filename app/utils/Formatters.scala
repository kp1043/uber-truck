package utils

import play.api.Play.current
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.data.validation.ValidationError
import models._
import dto.requests._
import scala.util.{Try, Success, Failure}
import java.util.UUID
import java.sql.Timestamp
import play.modules.reactivemongo.json.BSONFormats._

/**
 * Json Formatters for all classes
 */
object Formatters {
  
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext
  
  // Generates Writes and Reads for the given case classes
  
  /* read format for Point class */
  val pointReads = 
    (__ \ 'coordinates).read[Point](minLength[List[Double]](2).map(l => Point(l(0), l(1))))
    
  /* write format for Point class */
  val pointWrites = Writes[Point]( p => Json.obj(
      "type" -> "Point",
      "coordinates" -> List(p.lon, p.lat)
  ))
  
  implicit val pointFormat = Format(pointReads, pointWrites)
  
  
  implicit val errorResponseFormatter = Json.format[ErrorResponse]
  implicit val scheduleFormatter = Json.format[Schedule]
  implicit val truckFormatter = Json.format[Truck]
  implicit val truckCreateReqFormatter = Json.format[TruckCreateRequest]
  
  implicit val userGeocodeFormatter = Json.format[UserGeocode]
  implicit val searchPeriodFormatter = Json.format[SearchPeriod]
  implicit val truckSearchReqFormatter = Json.format[TruckSearchRequest]
  
  
  def uuidFormat : Format[UUID] = new Format[UUID] {
    def writes(uuid: UUID): JsString = JsString(uuid.toString)
    def reads(value: JsValue): JsResult[UUID] = value match {
      case JsString(x) => Try(UUID.fromString(x)) match {
            case Success(uuid) => JsSuccess(uuid)
            case Failure(msg) => JsError( __ \ 'UUID, ValidationError("validate.error.invalidUUID", msg) )
          }
      case _ => JsError( __ \ 'UUID, ValidationError("validate.error.invalidUUID", "Missing UUID String") )
    }
  }
  
}
