package utils

object Constants {
  
  /** The MongoDB collection name for Trucks */
  val TRUCKS_COLLECTION = "trucks"
    
  val EARTH_RADIUS_IN_MILES = 3959
  
}