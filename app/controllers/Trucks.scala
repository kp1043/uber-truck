package controllers

import java.util.UUID
import play.api.mvc._
import play.api.Logger
import play.api.libs.json._
import scala.concurrent.Future
import dto.requests._
import models._
import utils.Formatters._
import utils.ErrorResponse
import play.modules.reactivemongo.MongoController // Reactive Mongo plugin


/**
 * Defines the controller actions for the Truck resource
 */
object Trucks extends Controller with MongoController {
  
  /** 
   * CREATE: Action to create a new Truck 
   */
  def create() = Action.async(parse.json) { request =>
    Logger.info("Create request payload=${request.body.toString}")
    
    request.body.validate[TruckCreateRequest].fold(
      invalid = err =>
        Future.successful(BadRequest(ErrorResponse(BAD_REQUEST, s"Bad request payload. Error=$err").toJson)),
      req => 
        TruckDAO.save(req.toTruck).map(truck => Created(s"Created truck with id=${truck}"))
    )
  }
  
  
  /** 
   * READ: Action to show/GET a truck 
   */
  def show(id: UUID) = Action.async {
    //get the Future[Option[Truck]] object
    val futureTruckOption: Future[Option[Truck]] = TruckDAO.findById(id)

    futureTruckOption.map(truck =>
      if(truck.isDefined) Ok(truck.get.toJson)
      else BadRequest(ErrorResponse(BAD_REQUEST, s"Truck not found with id=$id").toJson)
    )
  }
  
  
  /**
   * SEARCH: Action to search trucks based on search parameters
   */
  def search() = Action.async(parse.json) { request =>
    Logger.info("Search request payload=${request.body.toString}")
    
    request.body.validate[TruckSearchRequest].fold(
      invalid = err => 
        Future.successful(BadRequest(ErrorResponse(BAD_REQUEST, s"Bad request payload. Error=$err").toJson)),
      req => 
        TruckDAO.search(req).map(trucks => Ok(Json.arr(trucks)))
    )
  }
  
  
  /**
   * UPDATE: Action to update a truck
   * Not supported for now
   */
  def update(id: UUID) = Action {
    NotImplemented("Update yet to be implemented")
  }
  
  
  /** 
   * DELETE: Action  to delete a truck 
   */
  def delete(id: UUID) = Action.async {
    TruckDAO.delete(id) map ( le =>
      if(le.ok) Ok(s"Deleted truck with id=$id") 
      else BadRequest(ErrorResponse(BAD_REQUEST, s"Delete failed with error=${le.errMsg}").toJson)
    )
  }
  
}