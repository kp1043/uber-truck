package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {
  
  def heartbeat() = Action {
    Ok("UberTruck Service is UP!")
  }
  
  def index = Action {
    Ok(views.html.index())
  }

}