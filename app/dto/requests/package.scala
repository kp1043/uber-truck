package dto

import play.api.libs.json._
import models._
import utils.Formatters._
import reactivemongo.bson.BSONObjectID
import java.util.UUID

/**
 * Data Transaction Request Objects
 */
package object requests {
  
  /**
   * Defines the request payload for creating a Truck resource using the POST end-point
   */
  case class TruckCreateRequest (
    locationId: String,
    name: String,
    facilityType: String,
    locationDescription: String,
    address: String,
    block: String,
    lot: String,
    permitId: String,
    permitStatus: String,
    foodItems: String,
    location: Point,
    schedules: List[Schedule]
  ) {
    
    def toJson: JsValue = Json.toJson(this)
    
    def toTruck() = {
      Truck(UUID.randomUUID(), locationId, name, facilityType,
            locationDescription, address, block, lot, permitId,
            permitStatus, foodItems, location, schedules)
    }
  }
  
  /**
   * Search end-point Request payload for Truck resource
   * Except the userGeocode, all other parameters are optional.
   * If a searchPeriod is provided, only trucks available (open) at any point within this 
   * period will be returned in search results.
   */
  case class TruckSearchRequest (
      userGeocode: UserGeocode,
      query: Option[String],
      facilityType: Option[String],
      period: Option[SearchPeriod]
  ) {
    
    def toJson: JsValue = Json.toJson(this)
  }
  
  /**
   * Defines the user location and search radius in miles
   */
  case class UserGeocode(
      long: Double,
      lat: Double,
      radiusInMiles: Double) {

    def toJson: JsValue = Json.toJson(this)
  }
  
  /**
   * Defines the time range for the search request
   * @startTime, @endTime:  UnixTimestamps in millis
   */
  case class SearchPeriod(
      startTime: Long,
      endTime: Long) {
    
    /* Validation for the search timestamps */
    require(startTime <= endTime, "searchStartTime must be <= searchEndTime") 
    
    def toJson: JsValue = Json.toJson(this)
  }
}